1. Apakah perbedaan antara JSON dan XML?
JSON merupakan format sedangkan XML adalah bahasa markup. JSON adalah format pertukaran data ringan yang jauh lebih mudah bagi komputer untuk memilah data, XML digunakan untuk menyimpan data dari satu aplikasi ke aplikasi lain.

2. Apakah perbedaan antara HTML dan XML?
XML fokus terhadap transfer data sedangkan HTML fokus terhadap penyajian data. HTML digunakan untuk menyajikan tampilan halaman web dan cara untuk menampilkan sebuah data, sementara XML lebih berperan sebagai transport data untuk membawa data agar dapat digunakan
