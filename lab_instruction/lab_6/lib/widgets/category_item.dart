import 'package:flutter/material.dart';

import '../screens/category_meals_screen.dart';

const MaterialColor indigo = const MaterialColor(
  0xFF3F51B5,
  const <int, Color>{
    50: const Color(0xFF3F51B5),
    100: const Color(0xFF3F51B5),
    200: const Color(0xFF3F51B5),
    300: const Color(0xFF3F51B5),
    400: const Color(0xFF3F51B5),
    500: const Color(0xFF3F51B5),
    600: const Color(0xFF3F51B5),
    700: const Color(0xFF3F51B5),
    800: const Color(0xFF3F51B5),
    900: const Color(0xFF3F51B5),

  },
);


class CategoryItem extends StatelessWidget {
  final String id;
  final String title;
  final Color color;

  CategoryItem(this.id, this.title, this.color);

  void selectCategory(BuildContext ctx) {
    Navigator.of(ctx).pushNamed(
      CategoryMealsScreen.routeName,
      arguments: {
        'id': id,
        'title': title,
      },
    );
  }

  @override
  Widget build(BuildContext context) {
    return InkWell(
      onTap: () => selectCategory(context),
      splashColor: Theme.of(context).primaryColor,
      borderRadius: BorderRadius.circular(15),
      child: Container(
        padding: const EdgeInsets.all(15),
        child: Text(
          title,
          style: Theme.of(context).textTheme.headline6,
        ),
        decoration: BoxDecoration(
          gradient: LinearGradient(
            colors: [
              indigo.withOpacity(0.7),
              indigo,
            ],
            begin: Alignment.topLeft,
            end: Alignment.bottomRight,
          ),
          borderRadius: BorderRadius.circular(15),
        ),
      ),
    );
  }
}
