from django.shortcuts import render
from lab_2.models import Note
from django.http.response import HttpResponseRedirect
from lab_4.forms import NoteForm

# Create your views here.
def index(request):
    notes = Note.objects.all().values()
    response = {'note' : notes}
    return render(request, 'lab4_index.html', response)

def add_note(request):
    responses = {}
    form = NoteForm(request.POST or None)
    if (form.is_valid() and request.method == 'POST') :
        form.save()
        return HttpResponseRedirect('/lab-4')
    
    responses['form'] = form
    return render(request, 'lab4_form.html', responses)

def note_list(request):
    notes = Note.objects.all().values()
    response = {'note' : notes}
    return render(request, 'lab4_note_list.html', response)